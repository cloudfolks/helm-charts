apiVersion: v1
kind: ServiceAccount
metadata:
  name: nfs-path-provisioner
  namespace: {{ .Values.platform.namespaces.system }}
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: nfs-path-provisioner-runner
rules:
  - apiGroups: [""]
    resources: ["persistentvolumes"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["persistentvolumeclaims"]
    verbs: ["get", "list", "watch", "update"]
  - apiGroups: ["storage.k8s.io"]
    resources: ["storageclasses"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["events"]
    verbs: ["create", "update", "patch"]
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: run-nfs-path-provisioner
subjects:
  - kind: ServiceAccount
    name: nfs-path-provisioner
    namespace: {{ .Values.platform.namespaces.system }}
roleRef:
  kind: ClusterRole
  name: nfs-path-provisioner-runner
  apiGroup: rbac.authorization.k8s.io
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: leader-locking-nfs-path-provisioner
  namespace: {{ .Values.platform.namespaces.system }}
rules:
  - apiGroups: [""]
    resources: ["endpoints"]
    verbs: ["get", "list", "watch", "create", "update", "patch"]
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: leader-locking-nfs-path-provisioner
  namespace: {{ .Values.platform.namespaces.system }}
subjects:
  - kind: ServiceAccount
    name: nfs-path-provisioner
    namespace: {{ .Values.platform.namespaces.system }}
roleRef:
  kind: Role
  name: leader-locking-nfs-path-provisioner
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nfs-path-provisioner
  labels:
    app: nfs-path-provisioner
  namespace: {{ .Values.platform.namespaces.system }}
spec:
  replicas: 1
  strategy:
    type: Recreate
  selector:
    matchLabels:
      app: nfs-path-provisioner
  template:
    metadata:
      labels:
        app: nfs-path-provisioner
    spec:
      serviceAccountName: nfs-path-provisioner
      containers:
        - name: nfs-path-provisioner
          image: {{ .Values.platform.services.nfsPathProvisioner.image }}:{{ .Values.platform.services.nfsPathProvisioner.tag }}
          volumeMounts:
            - name: nfs-client-root
              mountPath: /persistentvolumes
          env:
            - name: PROVISIONER_NAME
              value: tagbio-nfs-path-provisioner
            - name: NFS_SERVER
              value: {{ .Values.environment.nfs.host }}
            - name: NFS_PATH
              value: {{ .Values.environment.nfs.path }}
      volumes:
        - name: nfs-client-root
          nfs:
            server: {{ .Values.environment.nfs.host }}
            path: {{ .Values.environment.nfs.path }}
---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: tagbio-nfs-path-storage
provisioner: tagbio-nfs-path-provisioner
parameters:
  archiveOnDelete: "false"
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: test-claim
  annotations:
    volume.beta.kubernetes.io/storage-class: tagbio-nfs-path-storage
spec:
  storageClassName: tagbio-nfs-path-storage
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Mi
---
kind: Pod
apiVersion: v1
metadata:
  name: test-pod
spec:
  containers:
  - name: test-pod
    image: gcr.io/google_containers/busybox:1.24
    command:
      - "/bin/sh"
    args:
      - "-c"
      - "touch /mnt/SUCCESS && exit 0 || exit 1"
    volumeMounts:
      - name: nfs-pvc
        mountPath: "/mnt"
  restartPolicy: "Never"
  volumes:
    - name: nfs-pvc
      persistentVolumeClaim:
        claimName: test-claim
