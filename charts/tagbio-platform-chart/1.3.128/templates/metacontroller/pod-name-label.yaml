---
apiVersion: metacontroller.k8s.io/v1alpha1
kind: DecoratorController
metadata:
  name: service-per-pod
  namespace: tagbio-platform
spec:
  resources:
  - apiVersion: v1
    resource: pods
    annotationSelector:
      matchExpressions:
      - {key: service-per-pod-label, operator: Exists}
      - {key: service-per-pod-ports, operator: Exists}
  attachments:
  - apiVersion: v1
    resource: services
  hooks:
    sync:
      webhook:
        url: http://tagbio-metacontroller:8080/service-per-pod
    finalize:
      webhook:
        url: http://tagbio-metacontroller:8080/finalize-service-per-pod
---
apiVersion: metacontroller.k8s.io/v1alpha1
kind: DecoratorController
metadata:
  name: pod-name-label
  namespace: tagbio-platform
spec:
  resources:
  - apiVersion: v1
    resource: pods
    labelSelector:
      matchExpressions:
      - {key: pod-name, operator: DoesNotExist}
    annotationSelector:
      matchExpressions:
      - {key: pod-name-label, operator: Exists}
  hooks:
    sync:
      webhook:
        url: http://tagbio-metacontroller:8080/pod-name-label
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: tagbio-metacontroller-hooks
  namespace: tagbio-platform
data:
  pod-name-label.jsonnet: |-
    function(request) {
      local pod = request.object,
      local labelKey = pod.metadata.annotations["pod-name-label"],

      // Inject the Pod name as a label with the key requested in the annotation.
      labels: {
        [labelKey]: pod.metadata.name
      }
    }

  finalize-service-per-pod.jsonnet: |-
    function(request) {
      // If the StatefulSet is updated to no longer match our decorator selector,
      // or if the StatefulSet is deleted, clean up any attachments we made.
      attachments: [],
      // Mark as finalized once we observe all Services are gone.
      finalized: std.length(request.attachments['Service.v1']) == 0
    }

  service-per-pod.jsonnet: |-
    function(request) {
      local statefulset = request.object,
      local labelKey = statefulset.metadata.annotations["service-per-pod-label"],
      local ports = statefulset.metadata.annotations["service-per-pod-ports"],

      // Create a service for each Pod, with a selector on the given label key.
      attachments: [
        {
          apiVersion: "v1",
          kind: "Service",
          metadata: {
            name: statefulset.metadata.name,
            labels: {app: "service-per-pod"}
          },
          spec: {
            selector: {
              [labelKey]: statefulset.metadata.name
            },
            ports: [
              {
                local parts = std.split(portnums, ":"),
                port: std.parseInt(parts[0]),
                targetPort: std.parseInt(parts[1]),
              }
              for portnums in std.split(ports, ",")
            ]
          }
        }
      ]
    }
